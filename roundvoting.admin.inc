<?php
/**
 * @file
 * Admin functionality.
 */

/**
 * Update the voting totals for all rounds.
 */
function roundvoting_update_totals() {
  $time = time();
  $last_cron = variable_get('votingapi_last_cron', 0);
  $result = db_query('SELECT DISTINCT content_type, content_id FROM {votingapi_vote} WHERE timestamp > %d', $last_cron);

  while ($content = db_fetch_object($result)) {
    votingapi_recalculate_results($content->content_type, $content->content_id, TRUE);
  }

  variable_set('votingapi_last_cron', $time);
  drupal_set_message(t('Vote totals updated.'));
  drupal_goto('admin/content/node/results');
}

/**
 * List the results for a given round.
 */
function roundvoting_results() {
  // Build a list of all available rounds.
  $rounds = $links = array();
  $results = db_query("SELECT
      n.nid, n.title
    FROM
      node n
    WHERE n.type = 'roundvoting'
    ORDER BY n.created DESC");
  while ($result = db_fetch_object($results)) {
    $rounds[$result->nid] = $result->title;
    $links[] = l($result->title, 'admin/content/node/results', array('query' => array('round' => $result->nid)));
  }
  $output = theme('item_list', $links, NULL, 'ul', array('class' => 'item-list pager'));

  // Make sure a round is selected.
  $round = (isset($_GET['round']) && is_numeric($_GET['round'])) ? intval($_GET['round']) : 0;

  if (empty($round)) {
    $output .= 'Please select a round to see the results for that round.';
  }

  else {
    // Build a list of all results for this round.
    $rows = array();
    $per_page = 100;
    $page = (isset($_GET['page']) && is_numeric($_GET['page'])) ? intval($_GET['page']) : 0;
    $args = array($round);
    $query = "SELECT
        n.nid,
        n.title AS firstname,
        b.field_baby_lastinitial_value AS lastinitial,
        b.field_baby_birthday_value AS birthday,
        b.field_baby_gender_value AS gender,
        v.value
      FROM
        node n
        INNER JOIN content_field_round r
          ON n.nid = r.nid AND n.vid = r.vid
        INNER JOIN content_type_baby b
          ON n.nid = b.nid AND n.vid = b.vid
        LEFT OUTER JOIN votingapi_cache v
          ON n.nid = v.content_id
          AND r.field_round_nid = v.tag
      WHERE
        n.type = 'baby'
        AND r.field_round_nid = %d
        AND v.function = 'sum'
      ORDER BY
        v.value DESC";
    $results = pager_query($query, $per_page, 0, NULL, $args);
    while ($result = db_fetch_array($results)) {
      $result['nid'] = l($result['nid'], 'node/' . $result['nid']);
      $result['birthday'] = str_replace('T00:00:00', '', $result['birthday']);
      $rows[] = $result;
    }
    $header = array(
      t('ID'),
      t('First name'),
      t('Initial'),
      t('Birthday'),
      t('Gender'),
      t('Votes')
    );
    drupal_set_title(t('Voting results for @round (page @page)', array('@round' => $rounds[$round], '@page' => $page + 1)));
    $output .= theme('table', $header, $rows);
    $output .= theme('pager');
  }

  return $output;
}
