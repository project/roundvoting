RoundVoting
-----------
Allows for a voting on nodes to control voting on a node within a specific date
range, and with an optional number of votes per user.


Features
------------------------------------------------------------------------------
The primary features include:


Configuration
------------------------------------------------------------------------------


Credits / Contact
------------------------------------------------------------------------------
Currently maintained by Damien McKenna [1].

All development sponsored by Sandusky Phoenix [2].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  http://drupal.org/project/issues/roundvoting


References
------------------------------------------------------------------------------
1: http://drupal.org/user/108450
2: http://www.sanduskyphoenix.com/
