/**
 * @file
 * Custom JS for RoundVoting.
 */

// Global killswitch: only run if we are in a supported browser.
if (Drupal.jsEnabled) {
  // Documentation on Drupal JavaScript behaviors can be found here:
  // http://drupal.org/node/114774#javascript-behaviors
  Drupal.behaviors.roundvoting = function(context){
    jQuery('.' + Drupal.settings.roundvoting.widget_class + ':not(.roundvoting-processed)', context).addClass('roundvoting-processed').each(function(){
      var roundvoting_widget = jQuery(this);
      roundvoting_widget.find('.' + Drupal.settings.roundvoting.link_class).attr('href', function(){return jQuery(this).attr('href') + '&json=true';}).click(function(){
        jQuery.getJSON(jQuery(this).attr('href'), function(json){
          roundvoting_widget.find('.' + Drupal.settings.roundvoting.votesleft_class).hide().fadeIn('slow').html(json.votesleft);
          roundvoting_widget.find('.' + Drupal.settings.roundvoting.message_class).html(json.voted);
        });

        // Preventing the /roundvoting/vote/<nid> target from being triggered. 
        return false;
      });
    });
  };
}
